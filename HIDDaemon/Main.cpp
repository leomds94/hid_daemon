#include "Serial.hpp"
#include "Mouse.hpp"
#include "Keyboard.hpp"
#include <thread>
#include <cstdlib>

using namespace std;

Serial* serial;

void initSerial() {
	serial = new Serial();
}

int main()
{
	thread ts(initSerial);
	ts.join();

	while (true) {}

	return 0;
}