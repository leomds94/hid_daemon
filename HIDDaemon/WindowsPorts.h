#pragma once

#include <vector>
#include <string>

class WindowsPorts
{
public:
	WindowsPorts();
	~WindowsPorts();
	static std::vector<std::string> getPortNames();
};
