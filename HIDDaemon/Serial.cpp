#ifdef _WIN32
#define SERIAL_NAME "COM"
#define serialFlush(handle) PurgeComm(handle, PURGE_RXCLEAR | PURGE_TXCLEAR)
#elif __unix__
#define SERIAL_NAME "/dev/tty"

#define closePort(handle) 

#define serialFlush(handle) ioctl(handle, TCFLUSH, 2)
#endif

#include "Serial.hpp"

Serial::Serial()
{
	connected = false;

	vector<string> ports = WindowsPorts::getPortNames();

	for (int i = 0; i < ports.size(); i++) {
		try {
			//Try to connect to the given port
			hSerial = CreateFileA(ports[i].c_str(),
				GENERIC_READ | GENERIC_WRITE,
				0,
				NULL,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

			//Check if the connection was successfull
			if (hSerial == INVALID_HANDLE_VALUE)
			{
				//If not success full display an Error
				if (GetLastError() == ERROR_FILE_NOT_FOUND)
					printf("ERROR: Handle was not attached. Reason: %s not available.\n", ports[i].c_str());
				else
					printf("ERROR!!!");
			}
			else
			{
				// If connected we try to set the COM parameters
				DCB dcbSerialParams = { 0 };

				// Try to get the current
				if (!GetCommState(hSerial, &dcbSerialParams))
					printf("failed to get current serial parameters!");
				else
				{
					// Define serial connection parameters
					dcbSerialParams.BaudRate = CBR_115200;
					dcbSerialParams.ByteSize = 8;
					dcbSerialParams.StopBits = ONESTOPBIT;
					dcbSerialParams.Parity = NOPARITY;
					// Setting DTR to Control_Enable ensures that the controller is properly resetted
					dcbSerialParams.fDtrControl = DTR_CONTROL_ENABLE;

					// Set the parameters and check if it was successfull
					if (!SetCommState(hSerial, &dcbSerialParams))
						printf("ALERT: Could not set Serial Port parameters");
					else
					{
						connected = true;
						// Flush RX and TX buffers 
						serialFlush(hSerial);
						// Wait 2s as the controller will be reseting
						Sleep(2000);

						if (!DoHandShake())
							continue;
						else
							SendMouseAction();
					}
				}
			}
		}
		catch (exception ex) {
			std::cout << ports[i] << " not available! " << endl;
			CloseHandle(this->hSerial);
			this->connected = false;
			continue;
		}
	}
	cout << "No COM ports connected. Please connect one!" << endl;
}

Serial::~Serial()
{
	// Check if we are connected before trying to disconnect
	if (this->connected)
	{
		this->connected = false;
		CloseHandle(this->hSerial);
	}
}

bool Serial::DoHandShake() {

	// Get screen resolution
	int Width, Height;
	Mouse::GetDesktopResolution(Width, Height);
	string messageSend = "[HHID]:START;" + std::to_string(Width) + "@" +  std::to_string(Height);

	int tries = 0;
	size_t sent;
	string readmsg;
	// Try to get handshake successfully 3 times.
	do {
		std::cout << "Trying connection" << endl;
		sent = WriteData(messageSend);
		tries++;
		readmsg = ReadData();
	} while (readmsg != "[HHID]:STARTING" && tries <= 3);
	if (tries >= 3) {
		std::cout << "The driver tried the handshake, but didn't get response.\n" << endl;
		return false;
	}
	else
		return true;
}

void Serial::SendMouseAction() {

	string dataReceived;

	while (IsConnected()) {

		dataReceived = ReadData();
		try {
			POINT data = Mouse::ReadMovement(dataReceived);
			Mouse::UpdateMousePosition(data);
			continue;
		}
		catch (exception ex) {
			try {
				Mouse::MouseClickType clickType = Mouse::ReadMouseClick(dataReceived);
				Mouse::MouseClick(clickType);
				continue;
			}
			catch (exception ex) {
				std::cout << "No event detected\n";
				continue;
			}
		}
	}
}

std::string Serial::ReadData() {

	std::string readmsg = "";
	char c;
	DWORD bytesRead;

	ClearCommError(hSerial, &errors, &status);

	c = 'x';
	if (status.cbInQue > 0) {
		ReadFile(hSerial, &c, 1, &bytesRead, NULL);
		if (c == '\n' || c == '[') {
			if (c == '[')
				readmsg += c;
			while (c != '\r') {
				ReadFile(hSerial, &c, 1, &bytesRead, NULL);
				if (c != '\r' && c != '\n' && c != 'x')
					readmsg += c;
			}
		}
		else
			return ReadData();
	}
	else {
		Sleep(20);
		return ReadData();
	}
	return readmsg;
}


bool Serial::WriteData(std::string buf)
{
	DWORD bytesSend;

	//Try to write 'buf' on the Serial port
	if (!WriteFile(this->hSerial, buf.c_str(), (DWORD)buf.size(), &bytesSend, 0))
	{
		//In case it don't work get COM error and return false
		ClearCommError(this->hSerial, &this->errors, &this->status);
		return false;
	}
	else {
		Sleep(20);
		return true;
	}
}

bool Serial::IsConnected()
{
	return this->connected;
}
