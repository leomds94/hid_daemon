#ifndef _AFXDLL
#define _AFXDLL
#include <afxcoll.h>
#endif

#include "WindowsPorts.h"

WindowsPorts::WindowsPorts()
{
}
WindowsPorts::~WindowsPorts()
{
}

std::vector<std::string> WindowsPorts::getPortNames() {
	std::vector<std::string> portsArray;

	TCHAR lpTargetPath[5000]; // buffer to store the path of the COMPORTS
	DWORD test;

	for (int i = 0; i < 255; i++) // checking ports from COM0 to COM255
	{
		CString str;
		str.Format(_T("%d"), i);
		CString ComName = CString("COM") + CString(str); // converting to COM0, COM1, COM2

		test = QueryDosDevice(ComName, (LPWSTR)lpTargetPath, 5000);

		// Test the return value and error if any
		if (test != 0) //QueryDosDevice returns zero if it didn't find an object
		{
			std::wstring ws = std::wstring((LPCTSTR)ComName);
			std::string s = std::string(ws.begin(), ws.end());
			portsArray.push_back(s);
		}

		if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER) //in case buffer got filled
		{
			lpTargetPath[10000]; // increase size of the buffer.
			continue;
		}
	}
	return portsArray;
};