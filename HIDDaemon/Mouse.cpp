#include "Mouse.hpp"

using namespace std;

Mouse::Mouse() { }

void Mouse::GetDesktopResolution(int& horizontal, int& vertical) {
   RECT desktop;
   // Get a handle to the desktop window
   const HWND hDesktop = GetDesktopWindow();
   // Get the size of screen to the variable desktop
   GetWindowRect(hDesktop, &desktop);
   // The top left corner will have coordinates (0,0)
   // and the bottom right corner will have coordinates
   horizontal = desktop.right;
   vertical = desktop.bottom;
}

POINT Mouse::ReadMovement(string dataReceived) {
	long dx, dy;

	if (dataReceived.length() < 1) {
        fprintf(stderr, "No data received\n");
	}

	vector<string>positionStrings = Split::split(dataReceived, '@');

	try {
		dx = stol(string(positionStrings[0]));
		dy = stol(string(positionStrings[1]));
	}
	catch (exception e) {
        fprintf(stderr, "Data received was incorrect\n");
	}
	return POINT{ dx, dy };
}

Mouse::MouseClickType Mouse::ReadMouseClick(string dataReceived) {
	if (dataReceived.length() < 1) {
		fprintf(stderr, "No data received\n");
	}
	else {
		if (dataReceived[0] == '#') {
			switch (dataReceived[1]) {
			case '1':
				return MouseClickType::Left;
			case '2':
				return MouseClickType::Right;
			}
		}
		else {
			fprintf(stderr, "Data received was incorrect\n");
		}
	}
}

void Mouse::MouseClick(Mouse::MouseClickType clickType) {
	switch (clickType)
	{
	case Left:
		mouse_event((int)LeftDown, 0, 0, 0, 0);
		mouse_event((int)LeftUp, 0, 0, 0, 0);
		break;
	case Right:
		mouse_event(RightDown, 0, 0, 0, 0);
		mouse_event(RightUp, 0, 0, 0, 0);
		break;
	default:
		break;
	}
}

void Mouse::UpdateMousePosition(POINT movement) {
	POINT curPos;
    GetCursorPos(&curPos);
	SetCursorPos(movement.x, movement.y);
}