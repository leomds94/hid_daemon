#pragma once

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "Mouse.hpp"
#include "WindowsPorts.h"

class Serial
{
private:
	HANDLE hSerial;
	bool connected;
	//Get various information about the connection
	COMSTAT status;
	//Keep track of last error
	DWORD errors;

public:
	Serial();
	~Serial();

	std::string ReadData();

	bool WriteData(std::string buf);

	bool IsConnected();

	bool Serial::DoHandShake();

	void Serial::SendMouseAction();
};
