#pragma once

#include <Windows.h>
#include <vector>
#include "SplitExtension.cpp"

class Mouse
{
public:
	Mouse();

    enum MouseClickType {
		Left = 0, Right
	};

    static void GetDesktopResolution(int& horizontal, int& vertical);
    static POINT ReadMovement(string dataReceived);
	static MouseClickType ReadMouseClick(string dataReceived);

	static void MouseClick(MouseClickType clickType);
	static void UpdateMousePosition(POINT movement);

private:
	enum MouseEventFlags //Flags to identify mouse movement type.
	{
		LeftDown = 0x00000002,
		LeftUp = 0x00000004,
		MiddleDown = 0x00000020,
		MiddleUp = 0x00000040,
		Move = 0x00000001,
		Absolute = 0x00008000,
		RightDown = 0x00000008,
		RightUp = 0x00000010
	};
};

