#pragma once

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <vector>

class UnixPorts
{
public:
	UnixPorts();
	~UnixPorts();
	std::vector<std::string> getPortNames();
};

